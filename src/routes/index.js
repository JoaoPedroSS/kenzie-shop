import Cart from "../components/Cart";
import Products from "../components/products";

import { Switch, Route} from "react-router-dom";


const Routes = () => {
    return(
        <Switch>
            <Route exact path="/">
                <Products />
            </Route>
            <Route path="/carrinho">
                <Cart />
            </Route>
        </Switch>
    )
}

export default Routes;