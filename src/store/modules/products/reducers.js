
const defaultState = [
    {id: 1,name: "Echo Dot", price:  279.00, image: "http://assets.stickpng.com/images/5eb156a174bb7d0004ae61b6.png"},
	{id: 2, name: "Nootbook Acer", price: 2499.00, image: "https://static.kalunga.com.br/Anuncios/selos/222362/img/head-product.png"},
    {id: 3,name: "Iphone 12", price: 6499.00, image: "https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/iphone-12-pro-blue-hero?wid=940&hei=1112&fmt=png-alpha&.v=1604021661000"},
    {id: 4,name: "Televisão Samsung ", price: 1199.00, image: "https://images.samsung.com/is/image/samsung/africa-pt-full-hd-n5000-ua40n5000arxxa-frontblack-157755186?$720_576_PNG$"},
    {id: 5,name: "Geladeira", price: 1499.00, image: "https://images-americanas.b2w.io/produtos/01/00/img7/01/00/item/133605/7/133605724_1GG.png"},
]

const productsReducer = (state = defaultState, action) => {
    return state;
}

export default productsReducer;