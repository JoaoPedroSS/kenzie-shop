import { CART_ADD } from "./actionTypes";
import { CART_REMOVE } from "./actionTypes";

const cartReducer = (state = [], action) => {
  switch (action.type) {
    case CART_ADD:
      const { products } = action;
      return [...state, products];


    case CART_REMOVE:
        const { newList } = action;
        return newList;

    default:
      return state;
  }
};

export default cartReducer;