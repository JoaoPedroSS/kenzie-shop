import { CART_ADD } from "./actionTypes";

import { CART_REMOVE } from "./actionTypes";

export const cartAdd = (products) => ({
  type: CART_ADD,
  products,
});

export const cartRemove = (newList) => ({
  type: CART_REMOVE,
  newList,
});
