import { cartAdd, cartRemove } from "./actions";

export const cartAddThunk = (product) => {
  console.log(product);

  return (dispatch, getStore) => {
    const newList = JSON.parse(localStorage.getItem("cart")) || [];
    newList.push(product);
    localStorage.setItem("cart", JSON.stringify(newList));

    dispatch(cartAdd(product));
  };
};

export const cartRemoveThunk = (id) => (dispatch, getStore) => {
  const { cart } = getStore();
  const newList = cart.filter((product) => product.id !== id);
  localStorage.setItem("cart", JSON.stringify(newList));
  dispatch(cartRemove(newList));
};
