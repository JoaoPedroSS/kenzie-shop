
import "./App.css";
import Routes from "./routes";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <div style={{ display: "flex" }}>
          <Routes />
        </div>
      </header>
    </div>
  );
}

export default App;
