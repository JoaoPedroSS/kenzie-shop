import { useDispatch } from "react-redux";
import { cartAdd, cartRemove } from "../../store/modules/cart/actions";
import { cartAddThunk, cartRemoveThunk } from "../../store/modules/cart/thunks";
import "./product.css";

const Product = ({ products, isRemovable }) => {
  const { id, name, price, image } = products;

  const dispatch = useDispatch();

  return (
    <div className="box">
      <div className="box-products">
        <h3>{name}</h3>
        <h3>Valor: R$ {price}</h3>
        <img src={image} alt={name}></img>

        <div className="box-btn">
          {isRemovable ? (
            <button
              className="btn"
              onClick={() => dispatch(cartRemoveThunk(id))}
            >
              Remover item do carrinho
            </button>
          ) : (
            <button
              className="btn"
              onClick={() => dispatch(cartAddThunk(products))}
            >
              Adicionar item ao carrinho
            </button>
          )}
        </div>
      </div>
    </div>
  );
};

export default Product;
