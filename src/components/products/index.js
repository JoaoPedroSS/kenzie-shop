import Product from "../Product";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

const Products = () => {
  const products = useSelector((state) => state.products);

  const { cart } = useSelector((store) => store);

  return (
    <div>
      <div style={{ display: "flex" }}>
        {products.map((product) => (
          <Product key={product.id} products={product} />
        ))}
      </div>
      <div>
        <Link
          style={{
            cursor: "pointer",
            color: "white",
          }}
          to="/carrinho"
        >
          Carrinho
        </Link>
      </div>
      <br></br>
      <div>
        <p>items no Carrinho: {cart.length}</p>
      </div>
    </div>
  );
};

export default Products;
