import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

import Product from "../Product";
import "./cart.css";

const Cart = () => {
  const { cart } = useSelector((store) => store);

  return (
    <div>
      <div className="box-infos">
        <h4 className="titulo">Carrinho de compras</h4>
        <p className="item-add">Items Adicionados: {cart.length}</p>
        <p className="price-total">
          Valor Total: R$ {cart.reduce((soma, atual) => soma + atual.price, 0)}
        </p>
        <div>
          <Link
            style={{
              cursor: "pointer",
              color: "white",
            }}
            to="/"
          >
            Voltar
          </Link>
        </div>
      </div>
      {cart.map((product) => (
        <Product key={product.id} products={product} isRemovable />
      ))}
    </div>
  );
};

export default Cart;
